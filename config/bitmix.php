<?php

return [
    'developer_ips' => '82.78.230.101',
    'log_database_queries' => 'no',
    'faker_image_path' => storage_path("app/public/faker"),
];