<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Bitmixsoft\Laravel\Models\Admin;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('admins', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('email')->unique();;
                $table->string('password')->nullable();
                $table->text('google2fa_secret')->nullable()->default(null);
                $table->tinyInteger('google2fa_secret_passed')->unsigned()->default(0);
                $table->string('remember_token', 100)->nullable();
                $table->timestamp('lastlogin')->nullable();
                $table->boolean('status')->default(1)->nullable();
                
                $table->timestamps();
            });
            
            $role = Role::create(['name' => 'Superadmin', 'guard_name'=>'admin']);
            
            $default_admin = Admin::create([
                'name' => 'Default Admin',
                'email' => "admin@admin.com",
                'password' => Hash::make('password'),
                'status' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $default_admin->assignRole('Superadmin');
        } catch (Exception $e) {
            $this->down();
            throw new Exception($e->getMessage());
        }
        
    }
  
  
      /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::where(['name' => 'Superadmin', 'guard_name' => 'admin'])->delete();
        
        Schema::dropIfExists('admins');
    }
}
