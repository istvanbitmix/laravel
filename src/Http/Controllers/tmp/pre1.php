<?php
/*
auth null https://github.com/mpyw/null-auth
custom log viwer
*/
namespace App\Modules\Dev\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Artisan, DB, File, Config;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Database\Migrations\Migrator;

class DevController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('throttle:5,1', ['only' => 'login']);
        // $this->middleware(\App\Http\Middleware\Authenticate::class);

        /*$this->middleware(['auth:admin']); //protect admin, but it should be used in route too
        if (request()->ajax())
            \Debugbar::disable();
        if(setting('use_2fa')=="yes") {
            $this->middleware('barcode', ['except' => ['admin2fa', 'otp']]);
            $this->middleware('2fa', ['except' => ['admin2fa', 'getLogout']]);
        }

        if(config('admin.use_multilanguage') != 'yes') {
            \LaravelLocalization::setLocale(config('admin.default_language'));
        }*/
        foreach (app()->routes->getRoutes() as $route) {
            if(substr($route->uri, 0, 4)!="dev/" || $route->getName()=='' || strpos(strtolower($route->getName()), "maintenance.mode.") !==false || in_array(str_replace("dev.", "", $route->getName()), ['login', 'logout', 'debug.enable', 'debug.disable', 'debugbar.enable', 'debugbar.disable']))
                continue;
            $route_menus[$route->getName()] = ucfirst(str_replace([".", "_", "dev "], [" ", " ", ""], $route->getName()));
        }

        list($requirement_list, $requirements) = $this->laravelRequirements();
        asort($route_menus);
        \View::share('route_menus', $route_menus);
        \View::share('requirements', $requirements);
        \View::share('requirement_list', $requirement_list);
    }

    public function home() {
        $i = 1;
        $info[$i++]= "One Laravel install / tenancy";
        return view('Dev::info')->with(['info' => $info]);
    }

    public function login(Request $request) {
        //use encrypt to crypt a pwd
        // echo \Crypt::encryptString($request->input('otp'))."<br />";
        if ($request->isMethod('post') && $request->input('otp')!='' && $request->input('otp') ===  \Crypt::decryptString('eyJpdiI6ImVwYWNyRnl0OTZtK0hZbVh5eDB0MVE9PSIsInZhbHVlIjoieU1Pd2h2XC9CSVBwTGI5aG1RWFpJdXc9PSIsIm1hYyI6Ijk5YTE2MTViMWIyN2NmYjVhNjA4MTUwN2M0MGJmZTk3MGVjNDQyZGNhZjg1YTYyYjBlNDg4MTM5MDY1MjNjMzgifQ')) {
            $request->session()->put('authenticated', time());
            return redirect()->route('dev.home');
        }

        return view('Dev::login', [
            'message' => 'Provided PIN is invalid. ',
        ]);
        //Or, you can throw an exception here.
    }

    public function logout() {
        session()->forget(['authenticated']);
        return redirect()->route('dev.home');
    }


    public function migrateStatus() {
        Artisan::call("migrate:status", []);
        $migrate_raw = preg_split("/\n/", Artisan::output());
        $migrate_line[0] = [];
        if(is_array($migrate_raw) && count($migrate_raw)) {
            foreach ($migrate_raw as $key => $value) {
                if($key<=2)
                    continue;
                $migrate_line = preg_split("/\|/", $value);
                unset($migrate_line[0], $migrate_line[4]);
                $migrate_line = array_values($migrate_line);
                if(!isset($migrate_line[0]))
                    continue;
                $migrate_lines[] = ['migrated' => $migrate_line[0], 'migrate_file' => $migrate_line[1], 'bacth' => $migrate_line[2]];
            }
        }

        return view('Dev::migrate_status')->with(['migrate_lines' => $migrate_lines]);
    }

    function getModels($path){
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') continue;
            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $out = array_merge($out, getModels($filename));
            }else{
                $out[] = substr($filename,0,-4);
            }
        }
        return $out;
    }

    public function cacheClear() {
        $info = '';
        Artisan::call('view:clear');
        $info .= Artisan::output()."<br />";
        Artisan::call('cache:clear');
        $info .= Artisan::output()."<br />";
        Artisan::call('config:clear');
        $info .= Artisan::output()."<br />";
        Artisan::call('route:clear');
        $info .= Artisan::output()."<br />";

        if(class_exists(\Spatie\Permission\PermissionRegistrar::class)) {
            app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
            $info .= "\Spatie\Permission\PermissionRegistrar cache has been removed<br />";
        }

        if(class_exists(\Spatie\ResponseCache\Middlewares\CacheResponse::class)) {
            \Spatie\ResponseCache\Facades\ResponseCache::clear();
            $info .= "\Spatie\ResponseCache\Middlewares\CacheResponse cache has been removed<br />";
        }

        /*foreach (getModels(glob("/models/*")) as $key => $value) {
            $value->forgetAll();
        }*/

        File::delete(storage_path('app/modules/app.json'));
        $info .= "app/modules/app.json has been removed<br />";

        return view('Dev::info')->with(['info' => $info]);
    }


    public function routes() {
        return view('Dev::routes')->with(['routes' => app()->routes->getRoutes()]);
    }


    public function webserver() {
        $apache_modules =[];
        if (function_exists('apache_get_modules')) {
            $apache_modules = apache_get_modules();
            sort($apache_modules);
        }
        $apache_modules = implode(", ", $apache_modules);
        return view('Dev::webserver')->with(['apache_modules' => $apache_modules]);
    }


    public function environment() {
        $info = [];
        $env = $_ENV;
        ksort($env);
        foreach ($env as $option => $value)
            $info[$option] = $value;
        return view('Dev::info')->with(['info' => $info]);
    }

    public function phpini() {
        $info = [];
        foreach (ini_get_all(null, false) as $option => $value)
            $info[$option] = $value;
        return view('Dev::info')->with(['info' => $info]);
    }


    

    public function databaseTables() {
        $database_tables[0] = '';
        foreach(DB::select('SHOW TABLES') as $table)
            $database_tables[] = $table->{key($table)};
        unset($database_tables[0]);
        return view('Dev::info')->with(['info' => $database_tables]);
    }


    public function MaintenanceMode() {
        return view('Dev::maintanance_mode');
    }

    public function enableMaintenanceMode() {
        $maintenance_exclude_ips = explode(",", config('base.maintenance_mode_exclude_ips'));
        $maintenance_exclude_ips[] = \Request::ip();
        \Artisan::call('down', ['--allow'=>array_unique($maintenance_exclude_ips), '--message'=>__('Dev::general.Maintenance mode activated !!!')]);
        return redirect()->route('dev.maintenance.mode');
    }


    public function disableMaintenanceMode() {
        Artisan::call('up');
        return redirect()->route('dev.maintenance.mode');
    }


    public function laravelRequirements() {/*
        $laravelVersion = \App::VERSION();
        if(version_compare($laravelVersion, 6, ">") && version_compare($laravelVersion, 7, "<"))
            $laravelVersion = "6.0";
        $reqList = array(
            '5.3' => array(
                'php' => array(
                    '>=' => '5.6.4',
                    '<' => '7.2.0',
                ),
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.4' => array(
                'php' => '5.6.4',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.5' => array(
                'php' => '7.0.0',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => false,
                'json' => false,
                'obs' => ''
            ),
            '5.6' => array(
                'php' => '7.1.3',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'obs' => ''
            ),
            '5.7' => array(
                'php' => '7.1.3',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'obs' => ''
            ),
            '5.8' => array(
                'php' => '7.1.3',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'obs' => ''
            ),
            '6.0' => array(
                'php' => '7.2.0',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'ctype' => true,
                'json' => true,
                'bcmath' => true,
                'obs' => ''
            ),
        );

        // PHP Version
        if (is_array($reqList[$laravelVersion]['php'])) {
            $requirements['php_version'] = true;
            foreach ($reqList[$laravelVersion]['php'] as $operator => $version) {
                if ( ! version_compare(PHP_VERSION, $version, $operator)) {
                    $requirements['php_version'] = false;
                    break;
                }
            }
        }else{
            $requirements['php_version'] = version_compare(PHP_VERSION, $reqList[$laravelVersion]['php'], ">=");
        }

        // OpenSSL PHP Extension
        $requirements['openssl_enabled'] = extension_loaded("openssl");
        // PDO PHP Extension
        $requirements['pdo_enabled'] = defined('PDO::ATTR_DRIVER_NAME');
        // Mbstring PHP Extension
        $requirements['mbstring_enabled'] = extension_loaded("mbstring");
        // Tokenizer PHP Extension
        $requirements['tokenizer_enabled'] = extension_loaded("tokenizer");
        // XML PHP Extension
        $requirements['xml_enabled'] = extension_loaded("xml");
        // CTYPE PHP Extension
        $requirements['ctype_enabled'] = extension_loaded("ctype");
        // JSON PHP Extension
        $requirements['json_enabled'] = extension_loaded("json");
        // Mcrypt
        $requirements['mcrypt_enabled'] = extension_loaded("mcrypt_encrypt");
        // BCMath
        $requirements['bcmath_enabled'] = extension_loaded("bcmath");
        // mod_rewrite
        $requirements['mod_rewrite_enabled'] = null;
        if (function_exists('apache_get_modules')) {
            $requirements['mod_rewrite_enabled'] = in_array('mod_rewrite', apache_get_modules());
        }

        return array($reqList[$laravelVersion], $requirements);*/
        // return view('Dev::requirements')->with(['requirements' => $requirements, 'requirement_list' => $reqList[$laravelVersion]]);
    }

    public function speed() {
        function test_Math($count = 140000) {
            $time_start = microtime(true);
            $mathFunctions = array("abs", "acos", "asin", "atan", "bindec", "floor", "exp", "sin", "tan", "pi", "is_finite", "is_nan", "sqrt");
            foreach ($mathFunctions as $key => $function) {
                if (!function_exists($function)) unset($mathFunctions[$key]);
            }
            for ($i=0; $i < $count; $i++) {
                foreach ($mathFunctions as $function) {
                    $r = call_user_func_array($function, array($i));
                }
            }
            return number_format(microtime(true) - $time_start, 3);
        }
        function test_StringManipulation($count = 130000) {
            $time_start = microtime(true);
            $stringFunctions = array("addslashes", "chunk_split", "metaphone", "strip_tags", "md5", "sha1", "strtoupper", "strtolower", "strrev", "strlen", "soundex", "ord");
            foreach ($stringFunctions as $key => $function) {
                if (!function_exists($function)) unset($stringFunctions[$key]);
            }
            $string = "the quick brown fox jumps over the lazy dog";
            for ($i=0; $i < $count; $i++) {
                foreach ($stringFunctions as $function) {
                    $r = call_user_func_array($function, array($string));
                }
            }
            return number_format(microtime(true) - $time_start, 3);
        }
        function test_Loops($count = 19000000) {
            $time_start = microtime(true);
            for($i = 0; $i < $count; ++$i);
            $i = 0; while($i < $count) ++$i;
            return number_format(microtime(true) - $time_start, 3);
        }
        function test_IfElse($count = 9000000) {
            $time_start = microtime(true);
            for ($i=0; $i < $count; $i++) {
                if ($i == -1) {
                } elseif ($i == -2) {
                } else if ($i == -3) {
                }
            }
            return number_format(microtime(true) - $time_start, 3);
        }

        $total = 0;
        $functions = get_defined_functions();
        $line = str_pad("-",38,"-");

        $info = '';
        $info .= "<pre>$line\n|".str_pad("PHP BENCHMARK SCRIPT",36," ",STR_PAD_BOTH)."|\n$line\nStart : ".date("Y-m-d H:i:s")."\nServer : {$_SERVER['SERVER_NAME']}@{$_SERVER['SERVER_ADDR']}\nPHP version : ".PHP_VERSION."\nPlatform : ".PHP_OS. "\n$line\n";
        foreach ($functions['user'] as $user) {
            // if (preg_match('/app\\\modules\\\dev\\\http\\\controllers\\\test_/', $user)) {
            if (preg_match('/'.strtolower(str_replace("/", "\\\\", dirname(str_replace("\\", "/", get_class($this))))).'\\\test_/', $user)) {
                $total += $result = $user();
                $info .= str_pad(preg_replace("/.*\\\\/", "", $user), 25) . " : " . $result ." sec.\n";
            }
        }
        $info .= str_pad("-", 38, "-") . "\n" . str_pad("Total time:", 25) . " : " . $total ." sec.</pre>";
        return view('Dev::info')->with(['info' => $info]);
    }


    public function exec() {
        return view('Dev::info')->with(['info' => "Result for the shell command 'ls' are::<div class='border border-secondary p-3'>".nl2br(shell_exec('ls'))."</div>"]);
    }


    /*public function status() {
        $artisan_command = "module:list";
        Artisan::call($artisan_command);
        $module_list = Artisan::output();
        $m_exploded = explode("\n", $module_list);
        unset($m_exploded[count($m_exploded)-1], $m_exploded[count($m_exploded)-1], $m_exploded[0], $m_exploded[1], $m_exploded[2]);
        $m_exploded = array_values($m_exploded);

        $active_modules = count($m_exploded);
        foreach ($m_exploded as $ml) {
            $mll = array_map('trim', explode("|", $ml));
            if(isset($mll[5]) && strtolower($mll[5])!="enabled")
                $active_modules--;
        }
        echo "Modules: ".count($m_exploded)." (".$active_modules." active)";
        echo $this->nice_echo($artisan_command, $module_list);

        $artisan_command = "migrate:status";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);

        $artisan_command = "backup:list";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);

        $artisan_command = "schedule:list";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);


        $artisan_command = "route:list";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);
    }*/

    public function adminer() {
        Config::set('debugbar.enabled', false);
        $database_config = Config::get('database.default');
        $database_driver = Config::get("database.connections.$database_config.driver");
        if ($database_driver === "mysql")
            $database_driver = "server";

        if(!request()->username) {
            return redirect()->route('dev.adminer', [
                'driver' => $database_driver,
                'server' => Config::get("database.connections.$database_config.host"),
                'db' => Config::get("database.connections.$database_config.database"),
                'username' => Config::get("database.connections.$database_config.username"),
                'password' => Config::get("database.connections.$database_config.password"),
            ]);
        }

        require(rtrim(config('modules.locations.app.path'), "/")."/Dev/Resources/Views/adminer.php");
    }

    public function readurl() {
        $read_url = 'https://google.com';
        try {
            $read_test = file_get_contents($read_url);
            $info = 'ok';
        } catch (\Throwable $e) {
            $info = 'failed';
        }
        return view('Dev::info')->with(['info' => $read_url." ".$info]);
    }

    public function artisan() {
        $params = func_get_args();
        $artisan_parameters = [];
        if(isset($params[0])) {
            $args = explode("/", $params[0]);
            $command = $args[0];
            unset($args[0]);
            foreach ($args as $key => $value) {
                $vc = explode("=", $value);
                $artisan_parameters[$vc[0]] = $vc[1] ?? NULL;
            }
        } else
            $artisan_parameters[] = '-h';
        Artisan::call($command ?? "list", isset($command) ? $artisan_parameters : []);
        $info = '<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).'<br /><br /><br /></pre>';
        return view('Dev::info')->with(['info' => $info]);
    }



    public function composer($run="") {
        $composer_home = storage_path('composer');
        $composer_phar = base_path().'/composer.phar';
        $composer_workdir = storage_path('compotest');
        // find php executable
        //check writable for $composer_home/composer.json
        //check existance and writable for $composer_workdir

        if($run=="test_process_run_check_php_version")
            $process = new Process("php -v"); // test symphony process
        elseif(substr($run, 0, 7)=="require" || substr($run, 0, 7)=="install" || $run=="update") {
            $process = new Process(""); // test symphony process
            $process = $process->setCommandLine('export COMPOSER_HOME='.$composer_home.';php '.$composer_phar.' '.$run.' --no-interaction --working-dir '.$composer_workdir.'');
        }
        else
            die("Invalid command to run");
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        echo nl2br($process->getOutput());
    }


    public function getFolders()
    {
        $storage_path = storage_path('logs');
        $folders = glob($storage_path . '/*', GLOB_ONLYDIR);
        if (is_array($storage_path)) {
            foreach ($storage_path as $value) {
                $folders = array_merge(
                    $folders,
                    glob($value . '/*', GLOB_ONLYDIR)
                );
            }
        }

        if (is_array($folders)) {
            foreach ($folders as $k => $folder) {
                $folders[$k] = basename($folder);
            }
        }
        return array_values($folders);
    }

    /**
     * @param bool $basename
     * @return array
     */
    public function getFolderFiles($basename = false)
    {
        return $this->getFiles($basename, $this->folder);
    }

    public function getFiles($basename = false, $folder = '')
    {
        $pattern = '*.log';
        $files = glob(
            $this->storage_path . '/' . $folder . '/' . $pattern,
            preg_match($this->pattern->getPattern('files'), $pattern) ? GLOB_BRACE : 0
        );
        if (is_array($this->storage_path)) {
            foreach ($this->storage_path as $value) {
                $files = array_merge(
                  $files,
                  glob(
                      $value . '/' . $folder . '/' . $pattern,
                      preg_match($this->pattern->getPattern('files'), $pattern) ? GLOB_BRACE : 0
                  )
                );
            }
        }

        $files = array_reverse($files);
        $files = array_filter($files, 'is_file');
        if ($basename && is_array($files)) {
            foreach ($files as $k => $file) {
                $files[$k] = basename($file);
            }
        }
        return array_values($files);
    }

}