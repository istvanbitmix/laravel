<?php

namespace App\Modules\Zero\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Artisan;

class DeveloperController extends Controller
{

    public function __construct() {
        // $this->middleware('auth:admin'); //ensure nothing protected doesn't skip login screen
    }


    public function artisan_form() {
        $artisan_commands = Artisan::output(Artisan::call('list'));
        $artisan_commands_expl = preg_split("/\n/", $artisan_commands);
        $json_commands = "";
        foreach ($artisan_commands_expl as $command_line) {
            if(preg_match("/[a-z]:[a-z]/", $command_line, $garbage) == 1) {
                $command_line_expl = preg_split("/ /", $command_line);
                // $myarray=$command_line_expl;echo '<pre><font face="verdana" size="2">';print_r($myarray);echo __FILE__.":".__LINE__.'</font></pre>';
                $json_commands .= '{id: "'.$command_line_expl[2].'", name: "'.$command_line_expl[2]." : ".str_replace($command_line_expl[2], "", str_replace('"', '', $command_line)).'"},';
                // echo $command_line_expl[2]."<br />";
            } elseif(str_word_count($command_line)) {
                $json_commands .= '{id: "'.$command_line.'", name: "'.$command_line.'"},';
            }
        }
        $json_commands = substr($json_commands, 0, -1);


        return '
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tokeninput/1.7.0/jquery.tokeninput.min.js"></script>
        <link rel="stylesheet" type="text/css" href="'.url('/dashboard/css').'/token-input-facebook.css" />

        <script type="text/javascript">
        $(document).ready(function () {
            $(\'#command\').tokenInput([
                '.$json_commands.'
                ], {
                theme: "facebook",
                hintText: "Know of any cool games?",
                noResultsText: "Nothing found.",
                searchingText: "Gaming...",
                preventDuplicates: true,
                tokenLimit:1
            });
        });
        </script>

        <div style="text-align:center;margin:auto;width:500px;"><form method="post" action="'.url('dev/artisan').'" name="" id="" accept-charset="utf-8" style="font-family:monospace; font-size:12px;">
                <input type="hidden" name="_token" value="'.csrf_token().'" />
                Command: <input type="text" name="command" value="" id="command" style="width:200px" /><br />Options: <input type="text" name="command_options" value="" style="width:200px" /><br />
                <input type="submit" name="submit" value="Artisan!" class="submit" />
                </form></div>
        '.'<pre style="font-family:monospace; font-size:12px;">'.print_r($artisan_commands, true).__FILE__.":".__LINE__.'</pre>';
    }

    public function artisan(Request $request) {
        $inputs = $request->all();
        $action = \Request::route()->getAction();

        if(isset($action['command']))
            $inputs['command'] = $action['command'];

        $this->artisan_run($inputs['command']);
    }


    public function nice_echo($artisan_command, $str) {
        return '<br /><strong>'.$artisan_command.'</strong><pre style="font-family:monospace; font-size:12px;">'.print_r($str, true)."".'</pre>';
    }

    public function status() {
        $artisan_command = "module:list";
        Artisan::call($artisan_command);
        $module_list = Artisan::output();
        $m_exploded = explode("\n", $module_list);
        unset($m_exploded[count($m_exploded)-1], $m_exploded[count($m_exploded)-1], $m_exploded[0], $m_exploded[1], $m_exploded[2]);
        $m_exploded = array_values($m_exploded);

        $active_modules = count($m_exploded);
        foreach ($m_exploded as $ml) {
            $mll = array_map('trim', explode("|", $ml));
            if(isset($mll[5]) && strtolower($mll[5])!="enabled")
                $active_modules--;
        }
        echo "Modules: ".count($m_exploded)." (".$active_modules." active)";
        echo $this->nice_echo($artisan_command, $module_list);

        $artisan_command = "migrate:status";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);

        $artisan_command = "backup:list";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);

        $artisan_command = "schedule:list";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);


        $artisan_command = "route:list";
        Artisan::call($artisan_command);
        $artisan_output = Artisan::output();
        echo $this->nice_echo($artisan_command, $artisan_output);
    }

    public function artisan_run($command) {
        $extend_result = "";
        if($command == 'backup:run') {
            if(config('backup.notifications.mail.to') == 'your@example.com')
                config(['backup.notifications.mail.to' => '']);

            if(\Config::get('database.connections.'.\Config::get('database.default').'.database')=='homestead') //do not backup homestead DB
                config(['backup.backup.source.databases' => []]);
            $extend_result = "<a href=\"".url('../storage/app/'.config('app.name'))."\">Download backup</a><br />";
        }

        Artisan::call($command);
        // die('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).$extend_result."<br />".__FILE__.":".__LINE__.'</pre>');
        $artisan_output = '<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).$extend_result."<br />".__FILE__.":".__LINE__.'</pre>';
        return view('Zero::developer', compact('artisan_output'));
    }

    public function mysql() {
        $users = \DB::select('select * from migrations');
        $myarray=$users;echo '<pre><font face="verdana" size="2">';print_r($myarray);echo __FILE__.":".__LINE__.'</font></pre>';
    }

    public function phpinfo() {
        phpinfo();
    }

    public function exec() {
        echo "Executing shell command 'ls'. Results are:<br />";
        echo shell_exec('ls');
    }

    public function clear() {
        Artisan::call('cache:clear');
        print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
        Artisan::call('optimize');
        print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
        Artisan::call('view:clear');
        print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
        Artisan::call('route:clear');
        print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
        Artisan::call('module:optimize');
        print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
        // Artisan::call('config:cache');
        // print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
        // Artisan::call('route:cache');
        // print('<pre style="font-family:monospace; font-size:12px;">'.print_r(Artisan::output(), true).__FILE__.":".__LINE__.'</pre>');
    }

    public function mail() { // test google smtp and local server
        /*config(['mail.driver' => 'smtp']);
        config(['mail.host' => (\App::environment('local') ? 'smtp.gmail.com' : 'gmail-smtp-msa.l.google.com')]);
        config(['mail.port' => '587']);
        config(['mail.username' => 'istvan@bitmixsoft.com']);
        config(['mail.password' => '']);
        config(['mail.encryption' => 'tls']);*/

        /*config(['mail.driver' => 'sendmail']);
        config(['mail.host' => '']);
        config(['mail.port' => '']);
        config(['mail.username' => '']);
        config(['mail.password' => '']);
        config(['mail.encryption' => 'ssl']);*/

        echo "Mail sent: ";
        \Mail::send([], [], function($message) {
            $message->to('istvan@bitmixsoft.com');
            $message->subject('Test mail on '.url(''));
            $message->setBody("HTML mail test", 'text/html');
        });

    }

    public function laravel($laravelVersion) {
        $strOk   = '<i class="icon-ok">OK</i>';
        $strFail = '<i class="icon-remove">failed</i>';
        $strUnknown = '<i class="icon-question">question</i>';

        $reqList = array(
            '5.4' => array(
                'php' => '5.6.4',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'obs' => ''
            ),
            '5.5' => array(
                'php' => '7.0.0',
                'mcrypt' => false,
                'openssl' => true,
                'pdo' => true,
                'mbstring' => true,
                'tokenizer' => true,
                'xml' => true,
                'obs' => ''
            ),
        );

        $reqList['5.5'];
        // PHP Version
        $requirements['php_version'] = (version_compare(PHP_VERSION, $reqList[$laravelVersion]['php'] ,">=") >= 0);

        // OpenSSL PHP Extension
        $requirements['openssl_enabled'] = extension_loaded("openssl");

        // PDO PHP Extension
        $requirements['pdo_enabled'] = defined('PDO::ATTR_DRIVER_NAME');

        // Mbstring PHP Extension
        $requirements['mbstring_enabled'] = extension_loaded("mbstring");

        // Tokenizer PHP Extension
        $requirements['tokenizer_enabled'] = extension_loaded("tokenizer");

        // XML PHP Extension
        $requirements['xml_enabled'] = extension_loaded("xml");

        // Mcrypt
        $requirements['mcrypt_enabled'] = extension_loaded("mcrypt_encrypt");
        ?>

        <!-- <script src="https://use.fontawesome.com/96468c0c00.js"></script> -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <h1>Server Requirements.</h1>

        <p>
            PHP >= <?php echo $reqList[$laravelVersion]['php'] ?> <?php echo $requirements['php_version'] ? $strOk : $strFail; ?>
        </p>



        <?php if($reqList[$laravelVersion]['openssl']) : ?>
            <p>OpenSSL PHP Extension <?php echo $requirements['openssl_enabled'] ? $strOk : $strFail; ?></p>
        <?php endif; ?>

        <?php if($reqList[$laravelVersion]['pdo']) : ?>
            <p>PDO PHP Extension <?php echo $requirements['pdo_enabled'] ? $strOk : $strFail; ?></p>
        <?php endif ?>

        <?php if($reqList[$laravelVersion]['mbstring']) : ?>
            <p>Mbstring PHP Extension <?php echo $requirements['mbstring_enabled'] ? $strOk : $strFail; ?></p>
        <?php endif ?>

        <?php if($reqList[$laravelVersion]['tokenizer']) : ?>
            <p>Tokenizer PHP Extension <?php echo $requirements['tokenizer_enabled'] ? $strOk : $strFail; ?></p>
        <?php endif ?>


        <?php if($reqList[$laravelVersion]['xml']) : ?>
            <p>XML PHP Extension <?php echo $requirements['xml_enabled'] ? $strOk : $strFail; ?></p>
        <?php endif ?>

        <?php if($reqList[$laravelVersion]['mcrypt']) : ?>
            <p>Mcrypt PHP Extension <?php echo $requirements['mcrypt_enabled'] ? $strOk : $strFail; ?></p>
        <?php endif ?>

        <?php if(!empty($reqList[$laravelVersion]['obs'])): ?>
            <p class="obs"><?php echo $reqList[$laravelVersion]['obs'] ?></p>
        <?php endif; ?>
        <?php
    }

    public function maintenance_mode_enable() {
        if(config('base.maintenance_mode_exclude_ips')!='')
            $maintenance_exclude_ips = explode(",", config('base.maintenance_mode_exclude_ips'));
        $maintenance_exclude_ips[] = \Request::ip();
        \Artisan::call('down', ['--allow'=>array_unique($maintenance_exclude_ips), '--message'=>__('User::user.Maintenance mode activated!')]);
        return redirect()->route('home');
    }

    public function maintenance_mode_disable() {
        \Artisan::call('up');
        return redirect()->route('home');
    }
}
