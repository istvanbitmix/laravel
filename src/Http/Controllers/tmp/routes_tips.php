<?php

/*
composer require orchid/platform
php artisan orchid:install
https://www.talvbansal.me/blog/easy-push-to-deploy-for-laravel-with-gitlab-ci/
https://github.com/deployphp/deployer

*/

Route::get('/assets/{module}/{file}', [ function ($module, $file) { //https://georgebohnisch.com/serve-javascriptcss-laravel-route/
    try { //do only if the module exists
        \Debugbar::disable();
    } catch (\Throwable $e) {}

    $module = ucfirst($module);
    $path = app_path("Modules/$module/Resources/Assets/".$file);
    $mime = 'plain/txt';
    $mime_array = ['css'=>'text/css', 'js'=>'plain/txt'];
    if (\File::exists($path)) {
        $content = file_get_contents($path);
        $ext_arr = explode('.', $path);
        $ext = end($ext_arr);
        if(isset($mime_array[$ext]))
            $mime = $mime_array[$ext];
        return \Response::make($content, '200')->header('Content-Type', $mime);
    }
    return abort(404);
}])->where('file', '[A-Za-z0-9_/\-\.]+')->name('assets');




Route::get('/login-test', function() { echo __FILE__; })->middleware('auth:admin');
Route::get('/unprotected', function() { echo __FILE__; });


Route::get('/language/export', 'AdminController@language_export_csv')->name('admin.language.export');
Route::get('/language/import', 'AdminController@language_import_csv')->name('admin.language.import');
Route::get('/language/check', 'AdminController@language_check')->name('admin.language.check');
Route::get('/cache/clear', 'AdminController@cache_clear')->name('admin.cache.clear');

if(in_array(\Request::ip(), ["82.78.230.101" , "192.168.1.3", "192.168.1.6", '127.0.0.1'])) {
    $debug = Config::get('app.debug');
    Config::set('dev.initial_debug', $debug);
    $debugbar = Config::get('debugbar.enabled');
    Config::set('dev.initial_debugbar', $debugbar);
    if(File::exists(storage_path('debug-ip-').Request::getClientIp()))
        Config::set('app.debug', true);
    if(class_exists(Debugbar::class) && File::exists(storage_path('debugbar-ip-').Request::getClientIp()))
        Config::set('debugbar.enabled', true);
    Route::get('debug/off', function () { File::delete(storage_path('debug-ip-').Request::getClientIp()); return redirect()->back(); })->name('dev.debug.disable');
            Route::get('debug/on', function () { File::put(storage_path('debug-ip-').Request::getClientIp(), ''); return redirect()->back();})->name('dev.debug.enable');
            if(class_exists(Debugbar::class)) {
                Route::get('debugbar/off', function () { File::delete(storage_path('debugbar-ip-').Request::getClientIp()); return redirect()->back(); })->name('dev.debugbar.disable');
                Route::get('debugbar/on', function () { File::put(storage_path('debugbar-ip-').Request::getClientIp(), ''); return redirect()->back();})->name('dev.debugbar.enable');
            }

            Route::get('/', 'DevController@home')->name('dev.home');
            Route::get('routes', 'DevController@routes')->name('dev.routes');
            Route::get('migrate:status', 'DevController@migrateStatus')->name('dev.migrate.status');
            Route::get('php/phpinfo', 'DevController@phpinfo')->name('dev.php.phpinfo');
            Route::get('php/ini', 'DevController@phpini')->name('dev.php.ini');
            Route::get('webserver', 'DevController@webserver')->name('dev.webserver');
            Route::get('cache/clear', 'DevController@cacheClear')->name('dev.cache.clear');

            Route::get('maintenance', 'DevController@MaintenanceMode')->name('dev.maintenance.mode');
            Route::get('maintenance/enable', 'DevController@enableMaintenanceMode')->name('dev.maintenance.mode.enable');
            Route::get('maintenance/disable', 'DevController@disableMaintenanceMode')->name('dev.maintenance.mode.disable');
            Route::get('database/tables', 'DevController@databaseTables')->name('dev.database.tables');
            // Route::get('laravel/requirements', 'DevController@laravelRequirements')->name('dev.laravel.requirements');
            Route::any('adminer', 'DevController@adminer')->name('dev.adminer'); //require url except in verify token
            Route::any('environment', 'DevController@environment')->name('dev.environment');
            Route::any('test/exec', 'DevController@exec')->name('dev.test.exec');
            Route::any('test/speed', 'DevController@speed')->name('dev.test.speed');

            Route::any('test/readurl', 'DevController@readurl')->name('dev.test.readurl');

            Route::get('/artisan/{params?}', 'DevController@artisan')->where('params', '(.*)')->name('artisan'); // route:list/--sort=name route:list/--sort=name/-c  route:list/-h
}
