<?php

use Illuminate\Support\Facades\Route;

/*
Todo:
   - password protect every routes here, pasword from env or config  
   - add adminer
   - ad 2fa
   - view logs ...
   - admin menu
   - add developer middlaware maybe with a password protect
   - all function should goes int oo dec controller
   - betenni a r executable fileomat es azt valahogyan meghivni shell_exec-bol es az barmit megcsinal
*/
Route::get('/bitmixsoft', function () {
    dd('bitmixsoft');
});

Route::get('/dev/down-my-site', function(){
    return Artisan::call('down', ['--secret'=>'qwerty']);
})->name('down');

Route::get('/dev/up-my-site', function(){
    return Artisan::call('up');
})->name('up');
  
Route::group(['prefix'=>"test",'middleware'=>'null'], function () {
    // Route::get('/', ['middleware'=>'null', 'uses'=>'\Bitmixsoft\Laravel\App\Http\Controllers\DevController@index']);
});

Route::get('/backup/database',function(){
    $backup_file = str_replace(" ", "_", config('app.name'))."_db_backup_".date('Y-m-d_His').".zip";
    Artisan::call('backup:run', ['--only-db'=>true, '--filename'=> $backup_file]);
    $backup_file_path = storage_path('app/'.str_replace(" ", "-", config('app.name')).'/'.$backup_file);
    return Response::download($backup_file_path)->deleteFileAfterSend(true);
})->name('backup.database');
  

Route::group(['prefix'=>"dev", 'namespace' => 'Bitmixsoft\Laravel\Http\Controllers', 'middleware'=>'null', 'as' => 'dev.'], function () {
    Route::get('phpinfo', 'DevController@phpinfo')->name('phpinfo');
    
    if(class_exists(\Debugbar::class)) {
        Route::group(['prefix'=>"debugbar"], function () {
            Route::get('off', function () { File::delete(storage_path('debugbar-ip-').Request::ip()); File::put(storage_path('no-debugbar-ip-').Request::ip(), ''); })->name('dev.debugbar.disable');
            Route::get('on', function () { File::put(storage_path('debugbar-ip-').Request::ip(), ''); File::delete(storage_path('no-debugbar-ip-').Request::ip()); })->name('dev.debugbar.enable');
            Route::get('reset', function () { File::delete(storage_path('debugbar-ip-').Request::ip(), ''); File::delete(storage_path('no-debugbar-ip-').Request::ip(), ''); })->name('dev.debugbar.reset');
        });
        if(File::exists(storage_path('no-debugbar-ip-').Request::ip())) {
            \Debugbar::disable();
        } elseif(is_developer() || File::exists(storage_path('debugbar-ip-').Request::ip())) {
            config(['debugbar.enabled' => 1]);
            \Debugbar::enable();
        }
    }
    
    Route::get('storage:link', function () {
        Artisan::call('storage:link');
    });
    
});

