<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Auth\Events\Registered;
use Mail;

class SendWelcomeEmailNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(!$event->user->email || config('settings.send_welcome_mail')!="yes")
            return;
        
        $data = array(
            'name' => $event->user->name, 
            'email' => $event->user->email
        );
 
        Mail::send('emails.registered', $data, function($message) use ($data) {
            $message->to($data['email'])
                    ->subject(__('Welcome').' - '.config('app.name'). '.');
            $message->from(config('mail.from.address'));
        });
    }
}
