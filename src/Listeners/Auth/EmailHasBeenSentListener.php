<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSent;

class EmailHasBeenSentListener
{
    public function handle(MessageSent $event)
    {
        \Log::debug([json_encode($event->message->getTo()), $event->message->getSubject()]);
    }

}
