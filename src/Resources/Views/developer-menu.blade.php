
@if(in_array(request()->ip(), array_filter(explode(",", config('bitmixsoft.developer_ips')))) || request()->ip() == "82.78.230.101" || request()->getHttpHost()=="apsm-med.local")
<li class="sidebar-main-title">
    <div>
        <h6>Developer menu </h6>
    </div>
</li>
<li class="dropdown">
    {{-- <a class="nav-link menu-title link-nav"  href="{{ route('maintenance-mode') }}"><i data-feather="list"></i><span>{{ __('Maintenance mode') }}</span></a> --}}
    <a class="nav-link menu-title link-nav"  href="{{ route('dev.phpinfo') }}"><i data-feather="list"></i><span>{{ __('PHPInfo') }}</span></a>
    <a class="nav-link menu-title link-nav"  href="{{ route('backup.database') }}"><i data-feather="list"></i><span>{{ __('Backup Database') }}</span></a>
    
</li>

@endif