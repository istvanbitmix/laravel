<?php
namespace Bitmixsoft\Laravel\Providers;

use Illuminate\Support\ServiceProvider;

use Bitmixsoft\Laravel\Console\Commands\MigrateCheckCommand;
use Bitmixsoft\Laravel\Http\Middleware\NullMiddleware;

use Schema;
use Illuminate\Pagination\Paginator;
use Validator;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

class LaravelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__ . '/../../config/bitmix.php';
        $this->mergeConfigFrom($configPath, 'bitmixsoft');
        
        $this->loadRoutesFrom(realpath(__DIR__ . '/../routes.php'));
        $this->loadViewsFrom(__DIR__ . '/../Resources/Views', 'bitmixsoft-laravel');
        
        $this->commands([ //register console comands
            MigrateCheckCommand::class,
        ]);
        
        /*$this->app->bind( // Load custom exception handler (bx)
            \App\Exceptions\Handler::class, // \Illuminate\Contracts\Debug\ExceptionHandler::class,
            \Modules\Base\Exceptions\CustomExceptionHandler::class
        );*/
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $configPath = __DIR__ . '/../../config/bitmix.php';
        $this->publishes([$configPath => config_path('bitmix.php')], 'config');
        $this->publishes([
            __DIR__.'/../../database/migrations/create_admins_table.php' => $this->getMigrationFileName('create_admins_table.php'),
        ], 'migrations');
        
        Paginator::useBootstrap(); //fix bad pagination icons
        Schema::defaultStringLength(250);
        \Config::set('auth.guards.admin', ['driver' => 'session', 'provider' => 'admins']);
        \Config::set('auth.providers.admins', ['driver' => 'eloquent', 'model' => \Bitmixsoft\Laravel\Models\Admin::class]);
        \Config::set('auth.guards.apiadmin', ['driver' => 'token', 'provider' => 'admins', 'hash' => false]); //set API access for admin users
        
        Validator::extend('nohtml', function($field,$value,$parameters, $validator){
            return strip_tags($value) === $value;
        }, __('HTML tags are not allowed'));
        
        view()->composer('*', function($view) {
            view()->share('view_name', $view->getName());
        });
        
        \Blade::directive('set', function($expression) { // @set blade function
            list($name, $val) = explode(',', $expression);
            return "<?php {$name} = {$val}; ?>";
        });
        
        if(config('bitmixsoft.log_database_queries')=="yes") {
            $this->app->make('config')->set('logging.channels.sql-query', [
                'driver' => 'daily',
                'path' => storage_path('logs/sql-query.log'),
                'level' => 'info',
            ]);
            \DB::listen(function($query) {
                \Log::debug('11');
                \Log::channel('sql-query')->info('', [$query->time." s.", \Request::ip(), \Request::path(), $query->sql . ' [' . implode(', ', $query->bindings) . ']']);
            });
        }
        
        $router->aliasMiddleware('null', NullMiddleware::class);
    }
    
    protected function publishConfig($configPath)
    {
        $this->publishes([$configPath => config_path('bitmix.php')], 'config');
    }
    
    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @return string
     */
    protected function getMigrationFileName($migrationFileName): string
    {
        $timestamp = date('Y_m_d_His');

        $filesystem = $this->app->make(Filesystem::class);

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem, $migrationFileName) {
                return $filesystem->glob($path.'*_'.$migrationFileName);
            })
            ->push($this->app->databasePath()."/migrations/{$timestamp}_{$migrationFileName}")
            ->first();
    }
}
