<?php
namespace App\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class DefaultModelService
{
    public function __construct($model)
    {
        //model class
        $this->model = $model;
    }

    public function all($columns = ['*'])
    {
        return $this->model->get($columns);
    }

    public function lists($value, $key = null)
    {
        $lists = $this->model->pluck($value, $key);
        if (is_array($lists)) {
            return $lists;
        }
        return $lists->all();
    }

    public function paginate($perPage = 1, $columns = ['*'], $method = 'full')
    {
        $paginationMethod = $method !== 'full' ? 'simplePaginate' : 'paginate';
        return $this->model->$paginationMethod($perPage, $columns);
    }

    /**
     * create a model
     *
     * @param array $data data
     *
     * @return void
     */
    public function create(array $data)
    {
        $model = $this->model->create($data);
        return $model;
    }

    public function make(array $data)
    {
        return $this->model->make($data);
    }

    /**
     * update a model
     *
     * @param array $data data
     * @param mixed $id   id
     *
     * @return void
     */
    public function update(array $data, $id)
    {
        $model = $this->getById($id);
        if ($model != null) {
            $res = $model->update($data);
            return $res;
        }
        return null;
    }

    /**
     * delete a model
     *
     * @param int $id id
     * @return Model
     */
    public function delete($id)
    {
        $res =  $this->model->destroy($id);
        return $res;
    }

    public function getById($id)
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return null;
        }
    }

    public function find($id, $columns = ['*'])
    {
         return $this->model->find($id, $columns);
    }

    public function findBy($field, $value, $columns = ['*'])
    {
        return $this->model->where($field, '=', $value)->first($columns);
    }

    public function findAllBy($field, $value, $columns = ['*'])
    {
        return $this->model->where($field, '=', $value)->get($columns);
    }

}